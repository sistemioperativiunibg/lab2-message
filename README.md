# SIMPLE WAIT NOTIFY EXAMPLE #

Due thread paralleli `Sender` e `Receiver` si scambiano un messaggio e si sincronizzano tramite l'uso dei metodi `wait()` e `notify()`.

* se il `Receiver` non trova il messaggio si mette in `wait`
* il `Sender` dopo aver recapitato il messaggio esegue `notify()` e risveglia il `Receiver` se era stato precedentemente messo nel `wait set`